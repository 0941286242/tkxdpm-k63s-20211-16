# Phân công công việc

# 1. Usecase tổng quan
1.1. Đăng nhập/ Đăng ký (không làm)

1.2. Quản lý thông tin bãi xe               `Nguyễn Văn Tuấn`

1.3. Quản lý thông tin xe theo bãi          `Vũ Đức Hiếu`

1.4. Quản lý giao dịch thuê xe              `Nguyễn Anh Tuấn`

1.5. Thuê xe                                `Vũ Hoài Nam`

1.6. Cập nhật trạng thái thuê xe            `Kiều Minh Hướng`

1.7. Trả xe                                 `Đinh Đức Thắng`
