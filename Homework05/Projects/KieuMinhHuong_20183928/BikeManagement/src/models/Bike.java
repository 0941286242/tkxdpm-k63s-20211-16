package models;

public class Bike {
	
	private int id;
	private int type;
	private int status;
	private String description;
	
	private String bikeType;
	private String bikeStatus;
	
	
	public Bike(int id, int type, int status, String description) {
		super();
		this.id = id;
		this.type = type;
		this.status = status;
		this.description = description;
	}
	
	public Bike(int id, String bikeType, String bikeStatus, String description) {
		super();
		this.id = id;
		this.bikeType = bikeType;
		this.bikeStatus = bikeStatus;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBikeType() {
		return bikeType;
	}

	public void setBikeType(String bikeType) {
		this.bikeType = bikeType;
	}

	public String getBikeStatus() {
		return bikeStatus;
	}

	public void setBikeStatus(String bikeStatus) {
		this.bikeStatus = bikeStatus;
	}

}
