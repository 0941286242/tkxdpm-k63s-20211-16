package models;

public class Station {

	String name;
	String address;
	int totalBike;
	int availableBike;

	
	public Station(String name, String address, int totalBike, int availableBike) {
		super();
		this.name = name;
		this.address = address;
		this.totalBike = totalBike;
		this.availableBike = availableBike;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getTotalBike() {
		return totalBike;
	}

	public void setTotalBike(int totalBike) {
		this.totalBike = totalBike;
	}

	public int getAvailableBike() {
		return availableBike;
	}

	public void setAvailableBike(int availableBike) {
		this.availableBike = availableBike;
	}
	
	

}
