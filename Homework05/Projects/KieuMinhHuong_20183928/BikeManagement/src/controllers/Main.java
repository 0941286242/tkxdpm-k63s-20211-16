package controllers;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

	private Stage primaryStage;
	private Pane mainLayout;
	
	
	
	public void start(Stage primaryStage) throws IOException{
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Station");
		showMainView();
	}
	
	private void showMainView() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/MainView.fxml"));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	public void getAllStation(ActionEvent event) throws Exception {
		
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/StationView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);
	}
}
