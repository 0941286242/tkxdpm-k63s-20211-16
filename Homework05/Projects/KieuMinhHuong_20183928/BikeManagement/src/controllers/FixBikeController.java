package controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import helpers.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import models.Bike;
import models.Station;

public class FixBikeController implements Initializable{
	
	@FXML
	ComboBox<String> bikeType;
	@FXML
	ComboBox<String> bikeStatus;
	@FXML
	TextArea description;
	
	
	public void initialize(URL location, ResourceBundle resourses) {
		ObservableList<String> bikeTypeList = FXCollections.observableArrayList("Xe đạp đơn thường", "Xe đạp đơn điện", "Xe đạp đôi thường");
		ObservableList<String> bikeStatusList = FXCollections.observableArrayList("Chưa được mượn", "Đã được mượn");
		bikeType.setItems(bikeTypeList);
		bikeStatus.setItems(bikeStatusList);
	}

	
	public void fixBike(ActionEvent event) throws Exception {
		int bikeId = BikeController.choosedBikeId;
		DatabaseConnection connection = new DatabaseConnection();
		Connection connectDB = connection.getConnection();
		
		String query = "SELECT type, status, description FROM bike where id = " + bikeId;
		String query2 = "SELECT available_bike FROM station WHERE name = '" + StationController.choosedStationName + "'";
		
		try {
			Statement statement = connectDB.createStatement();
			ResultSet BikeOutput = statement.executeQuery(query);
			
			while(BikeOutput.next()) {
				int statusNow = BikeOutput.getInt(2);
				ResultSet stationOutput = statement.executeQuery(query2);
				int type = bikeType.getSelectionModel().getSelectedIndex();
				if(type == -1) type = 0;
				type += 1;
				int status = bikeStatus.getSelectionModel().getSelectedIndex();
				if(status == -1) status = 0;
				String descriptionText = description.getText();
				if (descriptionText.length() >= 50) {
					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText("Lỗi sửa mô tả");
					alert.setContentText("Mô tả không được vượt quá 50 ký tự.");
					alert.showAndWait();
					return;
				}
				while(stationOutput.next()) {
					int availableBike = stationOutput.getInt(1);
					if(statusNow != status) {
						if(status == 0) availableBike++;
						else availableBike--;
					}
					query = "UPDATE bike SET type = " + type + ", status = " + status + ", description = '" + descriptionText + "' WHERE id = " + bikeId;
					query2 = "UPDATE station SET available_bike = " + availableBike + " WHERE name = '" + StationController.choosedStationName + "'";
					statement.executeUpdate(query);
					statement.executeUpdate(query2);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		returnBikeView(event);
		
	}
	
	public void cancel(ActionEvent event) throws Exception{
		returnBikeView(event);
	}
	
	public void returnBikeView(ActionEvent event) {
		try {
			
			Parent root = FXMLLoader.load(getClass().getResource("/views/BikeView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
