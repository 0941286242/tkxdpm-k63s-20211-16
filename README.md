# tkxdpm_group16

## Phân công công việc:
1. Vũ Hoài Nam
- Làm usecase Thuê xe
- Tổng phần trăm đóng góp: 16,7%
2. Nguyễn Văn Tuấn
- Tổng phần trăm đóng góp: 16,7%
3. Nguyễn Anh Tuấn
- Làm module quản lí giao dịch thuê xe
- Tổng phần trăm đóng góp: 16,7%
4. Vũ Đức Hiếu
- Tổng phần trăm đóng góp: 16,7%
5. Đinh Đức Thắng
- Tổng phần trăm đóng góp: 16,7%
6. Kiều Minh Hướng
- Làm module Thêm, sửa, xóa xe trong bãi
- Tổng phần trăm đóng góp: 16,7%

## Kết quả chung: 
- Mỗi người đã hoàn thành những module của riêng mình
- Chưa ghép được code lại với nhau vì không thống nhất được cách thức xây dựng chương trình
