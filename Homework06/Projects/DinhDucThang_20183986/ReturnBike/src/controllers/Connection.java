package controllers;

import java.sql.DriverManager;

public class Connection {
	
	public Connection databaseLink;
	
	public Connection getConnection() {
		String databaseName = "ReturnBike";
		String databaseUser = "root";
		String databasePassword = "";
		String url = "jdbc:mysql://localhost/" + databaseName;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			databaseLink = (Connection) DriverManager.getConnection(url, databaseUser, databasePassword);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return databaseLink;
	}

}
