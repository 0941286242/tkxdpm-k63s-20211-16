package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.Bike;
import models.Card;

public class PaymentController implements Initializable {
	@FXML
	ChoiceBox<String> BXID;
	@FXML
	TextField CardHolderName;
	@FXML
	TextField CardNumber;
	
	public void initialize(URL location, ResourceBundle resourses) {
		BXID.setItems(FXCollections.observableArrayList(
			    "001", "002"));
	}
	
	Bike bike = new Bike();
	Random random = new Random();
	public int tgthue = 15*(random.nextInt(4));
	
	public void ReturnBike(ActionEvent event) throws Exception {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/ConfirmPaymentScreen.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int CalculateFee() {
		int x = 10000;
		
		int thoigianthue = tgthue;
		if(thoigianthue < 10) return 0;
		else {
			if(thoigianthue > 30) return x+x*thoigianthue*3/40;
			else return x;
		}
	}
	
	public void GetCustomerInfo() {
		Card card = new Card();
		card.setCardHolder(CardHolderName.getText());
		card.setCardNumber(CardNumber.getText());
	}

	public int gettgthue() {
		return tgthue;
	}
}
