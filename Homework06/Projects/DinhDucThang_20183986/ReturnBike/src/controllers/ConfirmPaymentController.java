package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

public class ConfirmPaymentController implements Initializable {
	@FXML
	Text BXid;
	@FXML
	Text CardHolder;
	@FXML
	Text CardNumber;
	@FXML
	Text Time;
	@FXML
	Text Type;
	@FXML
	Text Fee;
	@FXML
	Text ReturnFee;
	@FXML 
	Text StartFee;

	
	public void initialize(URL location, ResourceBundle resourses) {
		BXid.setText("001");
		Time.setText(Integer.toString(60));;
		Type.setText("Xe đạp");
		Fee.setText(Integer.toString(16000));
		StartFee.setText("200000");
		ReturnFee.setText(Integer.toString(200000-16000));
	}
		
}
