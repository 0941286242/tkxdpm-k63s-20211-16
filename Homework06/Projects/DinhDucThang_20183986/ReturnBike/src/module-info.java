module ReturnBike {
	requires javafx.controls;
	requires javafx.graphics;
	requires java.sql;
	requires javafx.fxml;
	
	opens controllers to javafx.graphics, javafx.fxml;
}
