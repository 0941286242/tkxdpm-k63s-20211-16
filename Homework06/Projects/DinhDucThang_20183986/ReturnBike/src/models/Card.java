package models;

public class Card {
	private String CardHolder;
	private String CardNumber;
	
	public String getCardHolder() {
		return CardHolder;
	}
	public void setCardHolder(String cardHolder) {
		CardHolder = cardHolder;
	}
	public String getCardNumber() {
		return CardNumber;
	}
	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}
}
