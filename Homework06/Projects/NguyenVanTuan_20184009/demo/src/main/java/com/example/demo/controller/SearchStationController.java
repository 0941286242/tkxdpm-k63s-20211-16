package com.example.demo.controller;

import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Station;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchStationController extends BaseController {

    private IStationDAO stationDAO;

    public SearchStationController() {
        this.stationDAO = new StationDAO();
    }

    /**
     * Search stations by searching information received from user
     * @param info: searching information
     * @return all stations if searching information is empty,
     * else return stations matching searching information
     */
    public List<Station> searchStations(HashMap<String, String> info) {
        String name = info.get("name");
        String address = info.get("address");
        String bikes = info.get("numBikes");
        String eBikes = info.get("numEBikes");
        String twinBikes = info.get("numTwinBikes");
        String emptyPositions = info.get("numEmptyPositions");
        List<Station> results;
        if (name == "" && address == "" && bikes == "0" && eBikes == "0"
                && twinBikes == "0" && emptyPositions == "0") {
            results = stationDAO.findAll();
            countBikesAndEmptyPositions(results);
            return results;
        }
        else {
            results = stationDAO.find(name, address);
            countBikesAndEmptyPositions(results);
            return filterNumOfBikesAndPositions(results, Integer.parseInt(bikes), Integer.parseInt(eBikes),
                    Integer.parseInt(twinBikes), Integer.parseInt(emptyPositions));
        }
    }

    /**
     * Count number of bikes, e-bikes, twin bikes and empty positions in stations.
     * Then set it to each station in the station list
     * @param stationList: the station list
     */
    public void countBikesAndEmptyPositions(List<Station> stationList) {
        for (Station station: stationList) {
            station.setNumOfBikes(stationDAO.countBikes(station.getId(), 1));
            station.setNumOfEBikes(stationDAO.countBikes(station.getId(), 2));
            station.setNumOfTwinBike(stationDAO.countBikes(station.getId(), 3));
            int emptyPositions = station.getTotalPositions() - station.getNumOfBikes()
                    - station.getNumOfEBikes() - station.getNumOfTwinBike();
            station.setNumOfEmptyPositions(emptyPositions);
        }
    }

    /**
     * Filter stations by minimum number of bikes, e-bikes, twin bikes and empty positions
     * basing on searching information
     * @param stationList: the station list that is needed to filter
     * @param bikes: minimum bikes number
     * @param eBikes: minimum e-bikes number
     * @param twinBikes: minimum twin bikes number
     * @param emptyPos: minimum empty positions number
     * @return the filtered station list
     */
    public List<Station> filterNumOfBikesAndPositions(List<Station> stationList, int bikes, int eBikes,
                                                      int twinBikes, int emptyPos) {
        List<Station> result = new ArrayList<>();
        for (Station station: stationList) {
            if (station.getNumOfBikes() >= bikes
                    && station.getNumOfEBikes() >= eBikes
                    && station.getNumOfTwinBike() >= twinBikes
                    && station.getNumOfEmptyPositions() >= emptyPos) {
                result.add(station);
            }
        }
        return result;
    }
}
