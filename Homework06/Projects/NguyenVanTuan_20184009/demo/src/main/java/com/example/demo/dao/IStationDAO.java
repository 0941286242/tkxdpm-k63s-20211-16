package com.example.demo.dao;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.Paginator;
import com.example.ecobikerental.entities.Station;

import java.util.List;

public interface IStationDAO extends IBaseDAO<Station> {
    public Paginator findAll(int skip, int maxResult);
    public Paginator find(String name, int skip, int maxResult);
    public int countEmptyPosition(int id);
    public Station find(int id);
    public Station findByName(String name);
    public Long createStation(Station station);
    public int countAll();
    public List<Station> findByDetails(int offset, int pageSize);

    /**
     * Find all stations
     * @return list of all stations
     */
    public List<Station> findAll();

    /**
     * Find stations by name and address
     *
     * @param name:    station name
     * @param address: station address
     * @return list of stations
     */
    public List<Station> find(String name, String address);

    /**
     * Count number of bikes in the station
     *
     * @param stationId:  The id of the station
     * @param categoryId: The id of category of bike
     * @return number of bikes in the station
     */
    public int countBikes(int stationId, int categoryId);
}
