package com.example.demo.dao;

import com.example.demo.entities.Station;

import java.util.List;



public class StationDAO extends BaseDAO<Station> implements IStationDAO {

    public int countAllPositions() {
        String command = "SELECT SUM(total_positions) FROM stations";
        return count(command );
    }

    public Paginator findAll(int skip, int maxResult) {
        String sqlCommand = "SELECT * FROM stations LIMIT ?, ?";
        List<Station> stations = super.query(sqlCommand, new StationMapper(), skip, maxResult);
        for (Station station : stations) {
            int bikes = countEmptyPosition(station.getId());
            station.setNumOfEmptyPositions(station.getTotalPositions() - bikes);
        }
        Paginator result = new Paginator();
        String count = "SELECT count(s.id) as total FROM stations s";
        result.setTotal(super.queryCount(count, "total"));
        result.setData(stations);
        return result;
    }

    @Override
    public Paginator find(String name, int skip, int maxResult) {
        String sqlCommand = "SELECT * FROM stations WHERE name LIKE '%" + name + "%' LIMIT ?, ?";
        List<Station> stations = super.query(sqlCommand, new StationMapper(), skip, maxResult);
        for (Station station : stations) {
            int bikes = countEmptyPosition(station.getId());
            station.setNumOfEmptyPositions(station.getTotalPositions() - bikes);
        }
        Paginator result = new Paginator();
        String count = "SELECT count(s.id) as total FROM stations s WHERE s.name LIKE '%" + name + "%'";
        result.setTotal(super.queryCount(count, "total"));
        result.setData(stations);
        return result;
    }

    @Override
    public int countEmptyPosition(int id) {
        String sqlCommand = "SELECT count(b.id) as 'empty' FROM bikes b WHERE b.station_id = ? AND b.state = ?";
        return super.queryCount(sqlCommand, "empty", id, State.AVAILABLE);
    }

    @Override
    public List<Station> findAll() {
        String sqlCommand = "SELECT * FROM stations";
        return super.query(sqlCommand, new StationMapper());
    }

    @Override
    public List<Station> find(String name, String address) {
        String sql = "SELECT * FROM stations WHERE name LIKE ? AND address LIKE ?";
        return query(sql, new StationMapper(), "%" + name + "%", "%" + address + "%");
    }

    @Override
    public int countBikes(int stationId, int categoryId) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM bikes JOIN stations ");
        sql.append("ON stations.id = bikes.station_id ");
        sql.append("JOIN categories ON bikes.category_id = categories.id ");
        sql.append("WHERE categories.id = ? AND stations.id = ?");
        return count(sql.toString(), categoryId, stationId);
    }

    @Override
    public Station find(int id) {
        String sqlCommand = "SELECT * FROM stations WHERE id = ?";
        List<Station> stations = super.query(sqlCommand, new StationMapper(), id);

        return stations.size() > 0 ? stations.get(0) : null;
    }

    @Override
    public Station findByName(String name) {
        String sqlCommand = "SELECT * FROM stations WHERE name = ?";
        List<Station> stations = super.query(sqlCommand, new StationMapper(), name);

        return stations.size() > 0 ? stations.get(0) : null;
    }

    @Override
    public Long createStation(Station station) {
        StringBuilder sql = new StringBuilder("INSERT INTO stations(name, address, image, total_positions, status, created_date) ");
        sql.append("VALUES(?, ?, ?, ?, ?, ?)");
        return insert(sql.toString(), station.getName(), station.getAddress(), station.getImage(),
                station.getTotalPositions(), station.getStatus(), station.getCreatedDate());
    }

    @Override
    public int countAll() {
        String command = "SELECT COUNT(*) FROM stations";
        return count(command);
    }

    @Override
    public List<Station> findByDetails(int offset, int pageSize) {
        String sql = "SELECT * FROM stations LIMIT ?, ?";
        return query(sql, new StationMapper(), offset, pageSize);
    }
}
