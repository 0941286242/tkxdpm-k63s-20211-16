package com.example.demo.dao.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.example.ecobikerental.utils.Configs.*;

public class MySQLConnection implements DBConnection {
    private static MySQLConnection instance = new MySQLConnection();

    public static MySQLConnection getInstance(){
        return instance;
    }

    public Connection setUpConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/" + DB_NAME + "?allowPublicKeyRetrieval=true&useSSL=false";
            return DriverManager.getConnection(url, DB_USERNAME, DB_PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
