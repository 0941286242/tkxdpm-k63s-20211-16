package com.example.demo.entities;
import javafx.scene.image.ImageView;

import java.util.List;

public class Station<Bike> extends BaseEntity{
    private String name;
    private String address;
    private int totalPositions;
    private String image;
    private int numOfBikes;
    private int numOfEBikes;
    private int numOfTwinBike;
    private int numOfEmptyPositions;
    private List<Integer> positions;
    private List<Bike> bikes;
    private ImageView imageView;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalPositions() {
        return totalPositions;
    }

    public void setTotalPositions(int totalPositions) {
        this.totalPositions = totalPositions;
    }

    public int getNumOfBikes() {
        return numOfBikes;
    }

    public void setNumOfBikes(int numOfBikes) {
        this.numOfBikes = numOfBikes;
    }

    public int getNumOfEBikes() {
        return numOfEBikes;
    }

    public void setNumOfEBikes(int numOfEBikes) {
        this.numOfEBikes = numOfEBikes;
    }

    public int getNumOfTwinBike() {
        return numOfTwinBike;
    }

    public void setNumOfTwinBike(int numOfTwinBike) {
        this.numOfTwinBike = numOfTwinBike;
    }

    public int getNumOfEmptyPositions() {
        return numOfEmptyPositions;
    }

    public void setNumOfEmptyPositions(int numOfEmptyPositions) {
        this.numOfEmptyPositions = numOfEmptyPositions;
    }

    public List<Integer> getPositions() {
        return positions;
    }

    public void setPositions(List<Integer> positions) {
        this.positions = positions;
    }

    public List<Bike> getBikes() {
        return bikes;
    }

    public void setBikes(List<Bike> bikes) {
        this.bikes = bikes;
    }

    public String toString() {
        return name;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
}
