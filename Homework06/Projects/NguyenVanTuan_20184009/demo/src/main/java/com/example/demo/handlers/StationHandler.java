package com.example.demo.handlers;


import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class StationHandler extends UserBaseHandler {
    @FXML
    protected Text name;
    @FXML
    protected Text positionAvailable;
    @FXML
    protected Text address;
    @FXML
    protected JFXButton chooseBtn;
    @FXML
    protected ImageView image;

    private Station station;
    private Stage stage;
    private ReturnBikeController returnBikeController;
    private ChooseStationHandler chooseStationHandler;

    public StationHandler(String screenPath, Station station, Stage stage, ReturnBikeController returnBikeController, ChooseStationHandler chooseStationHandler) throws IOException {
        super(stage, screenPath);
        this.station = station;
        this.stage = stage;
        this.returnBikeController = returnBikeController;
        this.chooseStationHandler = chooseStationHandler;
        setStationInfo();
        chooseBtn.setOnMouseClicked(event -> {
            //move to choose position screen
            if(station.getNumOfEmptyPositions() > 0){
                try {
                    ChoosePositionHandler positionHandler = new ChoosePositionHandler(this.stage, Configs.CHOOSE_POSITION_PATH);
                    positionHandler.setBController(returnBikeController);
                    positionHandler.setHomeHandler(homeHandler);
                    positionHandler.setPreviousScreen(chooseStationHandler);
                    positionHandler.setScreenTitle("Choose Position");
                    positionHandler.setStation(station);
                    positionHandler.setRental(chooseStationHandler.getRental());
                    positionHandler.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {

            }
        });
    }

    private void setStationInfo(){
        name.setText(station.getName());
        address.setText(station.getAddress());
        positionAvailable.setText(String.valueOf(station.getNumOfEmptyPositions()));
        System.out.println(station.getImage());
        setImage(image, station.getImage());
    }
}
