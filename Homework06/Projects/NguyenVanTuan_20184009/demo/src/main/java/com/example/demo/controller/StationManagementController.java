package com.example.demo.controller;

import com.example.ecobikerental.App;
import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;

public class StationManagementController extends BaseController {

    public final IStationDAO stationDAO;

    public StationManagementController() {
        this.stationDAO = new StationDAO();
    }

    public int countTotalPages(int pageSize) {
        int numberOfStation = stationDAO.countAll();
        return numberOfStation % pageSize == 0 ? numberOfStation/pageSize : numberOfStation/pageSize+1;
    }

    public void initializeStationTablePage(TableView<Station> stationTable, int offset, int pageSize) {
        List<Station> stations = stationDAO.findByDetails(offset, pageSize);
        for (Station station : stations) {
            station.setImageView(new ImageView(new Image(String.valueOf(App.class.getResource(station.getImage())), 45, 30, false, true)));
        }
        ObservableList<Station> stationItems = FXCollections.observableList(stations);
        stationTable.setItems(stationItems);
    }
}
