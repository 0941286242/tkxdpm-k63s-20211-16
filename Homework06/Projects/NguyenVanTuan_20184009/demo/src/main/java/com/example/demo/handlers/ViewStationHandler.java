package com.example.demo.handlers;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ViewStationHandler extends UserBaseHandler implements Initializable {

    @FXML
    private ImageView image;

    @FXML
    private Text stationName;

    @FXML
    private Text address;

    @FXML
    private Text bikes;

    @FXML
    private Text eBikes;

    @FXML
    private Text twinBikes;

    @FXML
    private Text positions;

    @FXML
    private Button btnViewBikes;

    @FXML
    private Button btnBack;

    private Station station;

    public ViewStationHandler(Stage stage, String screenPath, Station station) throws IOException {
        super(stage, screenPath);
        displayStation(station);
        this.station = station;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        this.btnViewBikes.setOnMouseClicked(e -> {
            try {
                ChooseBikeHandler chooseBikeHandler = new ChooseBikeHandler(this.stage, Configs.CHOOSE_BIKE_PATH);
                chooseBikeHandler.setBController(new ReturnBikeController());
                chooseBikeHandler.setHomeHandler(homeHandler);
                chooseBikeHandler.setPreviousScreen(this);
                chooseBikeHandler.setScreenTitle(station.getName());
                chooseBikeHandler.setStation(station);
                chooseBikeHandler.show();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        this.btnBack.setOnMouseClicked(e -> {
            this.getPreviousScreen().show();
        });
    }

    public void displayStation(Station station) {
        this.stationName.setText(station.getName());
        this.address.setText(station.getAddress());
        this.bikes.setText(String.valueOf(station.getNumOfBikes()));
        this.eBikes.setText(String.valueOf(station.getNumOfEBikes()));
        this.twinBikes.setText(String.valueOf(station.getNumOfEBikes()));
        this.positions.setText(String.valueOf(station.getNumOfEmptyPositions()));
        setImage(this.image, station.getImage());
    }
}
