package com.example.demo.controller;

import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.exceptions.InvalidStationInfoException;

import java.sql.Timestamp;
import java.text.Normalizer;
import java.util.Hashtable;
import java.util.regex.Pattern;

public class StationController extends BaseController {

    private final IStationDAO stationDAO;

    public StationController() {
        stationDAO = new StationDAO();
    }

    public void addStation(Hashtable<String, String> stationInfo) {
        validateStationInfo(stationInfo);
        checkNameExist(stationInfo.get("name"));

        Station station = new Station();
        station.setName(stationInfo.get("name"));
        station.setAddress(stationInfo.get("address"));
        station.setTotalPositions(30);
        station.setImage(stationInfo.get("image"));
        station.setStatus(1);
        Timestamp createdDate = new Timestamp(System.currentTimeMillis());
        station.setCreatedDate(createdDate);
        stationDAO.createStation(station);
    }

    public void validateStationInfo(Hashtable<String, String> stationInfo) {
        if (!validateStationName(stationInfo.get("name")))
            throw new InvalidStationInfoException("Invalid Station Name!");
        if (!validateStationAddress(stationInfo.get("address")))
            throw new InvalidStationInfoException("Invalid Station Address!");
        if (!validateStationTotalPositions(stationInfo.get("total_positions")))
            throw new InvalidStationInfoException("Invalid Station Total Positions!");
    }

    public boolean validateStationTotalPositions(String totalPositions) {
        if (totalPositions == null) return false;
        try {
            Integer.parseInt(totalPositions);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean validateStationName(String name) {
        if (name == null || name.equals("")) return false;
        String normalName = deAccent(name);
        for (int i = 0; i < normalName.length(); i++) {
            char c = normalName.charAt(i);
            if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' '))
                return false;
        }
        return true;
    }

    public boolean validateStationAddress(String address) {
        if (address == null || address.equals("")) return false;
        String normalAddress = deAccent(address);
        for (int i = 0; i < normalAddress.length(); i++) {
            char c = normalAddress.charAt(i);
            if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')
                    || c == ' ' || c == ','))
                return false;
        }
        return true;
    }

    public void checkNameExist(String name) {
        if (stationDAO.findByName(name) != null)
            throw new InvalidStationInfoException("Station Name Not Available!");
    }

    public static String deAccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }
}
