package com.example.demo.handlers;



import com.example.demo.entities.Station;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ChooseStationHandler  implements Initializable {
    @FXML
    private TextField searchInput;
    @FXML
    private Button searchBtn;
    @FXML
    private Pagination paging;
    @FXML
    private HBox hBox;
    @FXML
    private VBox vBox1;
    @FXML
    private VBox vBox2;

    private Rental rental;
    private List<Station> stations;
    private List stationComponents;
    private int total;

    public ChooseStationHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);

        searchBtn.setOnMouseClicked(event -> {
            paging.setPageFactory(new Callback<Integer, Node>(){
                @Override
                public Node call(Integer param) {
                    getPageStations(param);
                    return hBox;
                }
            } );
        });

        paging.setPageFactory(new Callback<Integer, Node>(){
            @Override
            public Node call(Integer param) {
                getPageStations(param);
                return hBox;
            }
        } );
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    public void init(){
        getPageStations(0);
    }

    private void getPageStations(int pageIndex){
        Paginator pagingEntity = getStations(searchInput.getText(), pageIndex*Configs.PER_PAGE);
        this.stations = pagingEntity.getData();
        stationComponents = new ArrayList();
        try {
            for(Station s: this.stations){
                StationHandler station = new StationHandler(Configs.STATION_PATH, s, this.stage, (ReturnBikeController) getBController(), this);
                stationComponents.add(station);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        addStation(this.stationComponents);
        this.total = pagingEntity.getTotal();
        int totalPage = this.total%Configs.PER_PAGE == 0?this.total/Configs.PER_PAGE :(this.total/Configs.PER_PAGE + 1);
        paging.setPageCount(totalPage);
        paging.setCurrentPageIndex(pageIndex);
    }

    private void addStation(List<StationHandler> items){
        ArrayList stationItems = (ArrayList)((ArrayList) items).clone();
        hBox.getChildren().forEach(node -> {
            VBox vBox = (VBox) node;
            vBox.getChildren().clear();
        });
        while(!stationItems.isEmpty()){
            hBox.getChildren().forEach(node -> {
                VBox vBox = (VBox) node;
                while (vBox.getChildren().size() < 2 && !stationItems.isEmpty()){
                    StationHandler stationHandler = (StationHandler)stationItems.get(0);
                    vBox.getChildren().add(stationHandler.getContent());
                    stationItems.remove(stationHandler);
                }
            });
            return;
        }
    }

    private Paginator getStations(String name, int skip){
        ReturnBikeController controller = (ReturnBikeController) getBController();
        Paginator result;
        if(name == null || name.equals("")){
            result = controller.getStations(skip, Configs.PER_PAGE);
        } else {
            result = controller.getStations(name, skip, Configs.PER_PAGE);
        }
        return result;
    }

    public void setRental(Rental rental) {
        this.rental = rental;
    }
    public Rental getRental(){
        return rental;
    }
}
