package com.example.demo.dao.database;

import java.sql.Connection;

public interface DBConnection {
    Connection setUpConnection();
}
