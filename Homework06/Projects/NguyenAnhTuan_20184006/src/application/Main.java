package application;
	
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.paint.Color;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;



public class Main extends Application implements Initializable {
	
	@FXML
    private TableColumn<StatusBike, Integer> id;

    @FXML
    private TableColumn<StatusBike, String> name;

//    @FXML
//    private TableColumn<?, ?> action;

    @FXML
    private Button buttonInfo1;
    
    @FXML
    private Button buttonInfo2;

    @FXML
    private Button buttonInfo3;
    
    @FXML
    private Button buttonUpdate1;
    
    @FXML
    private Button buttonUpdate2;

    @FXML
    private Button buttonUpdate3;

    @FXML
    private TableView<StatusBike> table;

    @FXML
    private TableColumn<StatusBike, String> status;
    
    @FXML
    private Button refresh;
    
    ObservableList<StatusBike> list = FXCollections.observableArrayList();
    
    public static Connection getMyConnection() throws SQLException,
     ClassNotFoundException {
		return MySQLConnection.getMySQLConnection();
    } 
    
    public void refreshTable() throws SQLException,
    ClassNotFoundException {
    	table.getItems().clear();
    	try {
    		getDataStatusBike();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void getDataStatusBike() throws SQLException,
    ClassNotFoundException {
    	Connection conn = Main.getMyConnection();
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery("select * from statusbike");
    	while (rs.next()) {
    		if(checkStatus(rs.getString(4), "false")) {
    			list.add(new StatusBike(rs.getInt(2), rs.getString(3),"Đã cho thuê"));
    		} else {
    			list.add(new StatusBike(rs.getInt(2), rs.getString(3),"Chưa cho thuê"));
    		}     
        }
    }
    
    public boolean checkStatus(String a, String b)  {
    	if(a.compareTo(b) == 0) {
    		return true;
    	} else {
    		return false;
    	}
    	
    }
   
    
    public void showInfoBike(ActionEvent e) throws SQLException,
    ClassNotFoundException {
    	Connection conn = Main.getMyConnection();
    	PreparedStatement stmt = conn.prepareStatement(
    			"select * from statusbike where bikeid = ?");
    	Node node = (Node) e.getSource() ;
        String data = (String) node.getUserData();
        int value = Integer.parseInt(data);
    	stmt.setInt(1, value);
    	ResultSet rs = stmt.executeQuery();
    	while (rs.next()) {
    		if(checkStatus(rs.getString(4), "false")) {
    			Alert alert = new Alert(Alert.AlertType.ERROR);
    			alert.setHeaderText("Chưa có người thuê!");
    			alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea("Error")));
    			alert.showAndWait();
    		} else {
		    	Parent root1;
		        try {
		        	FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/showInfoBike.fxml"));
		            loader.getNamespace().put("id", value);
		            root1 = loader.load();
		            Stage stage = new Stage();
		            Scene sceneInfoBike = new Scene(root1, Color.WHITE);
		            stage.setTitle("Info Bike");
		            stage.setScene(sceneInfoBike);
		            stage.show();
		        }
		        catch (Exception error) {
		            error.printStackTrace();
		        }
    		}
    	}
    }
    
    public void updateInfoBike(ActionEvent e) throws SQLException,
    ClassNotFoundException {
    	Connection conn = Main.getMyConnection();
    	PreparedStatement stmt = conn.prepareStatement(
    			"select * from statusbike where bikeid = ?");
    	Node node = (Node) e.getSource() ;
        String data = (String) node.getUserData();
        int value = Integer.parseInt(data);
    	stmt.setInt(1, value);
    	ResultSet rs = stmt.executeQuery();
    	while (rs.next()) {
    		if(rs.getString(4).compareTo("true") == 0) {
    			Alert alert = new Alert(Alert.AlertType.ERROR);
    			alert.setHeaderText("Đã có người thuê!");
    			alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea("Error")));
    			alert.showAndWait();
    		} else {
        		Parent root1;
    	        try {
    	        	FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/updateInfoBike.fxml"));
		            loader.getNamespace().put("id", value);
		            root1 = loader.load();
    	            Stage stage = new Stage();
    	            Scene sceneInfoBike = new Scene(root1, Color.WHITE);
    	            stage.setTitle("Update Bike");
    	            stage.setScene(sceneInfoBike);
    	            stage.show();
    	        }
    	        catch (Exception error) {
    	            error.printStackTrace();
    	        }
        	}
        }
    	 
    	
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	id.setCellValueFactory(new PropertyValueFactory<StatusBike, Integer>("id"));
    	name.setCellValueFactory(new PropertyValueFactory<StatusBike, String>("name"));
    	status.setCellValueFactory(new PropertyValueFactory<StatusBike, String>("status"));
    	
    	try {
    		getDataStatusBike();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	table.setItems(list);
    }
	
	@Override
	public void start(Stage stage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/main.fxml"));
			Scene scene1 = new Scene(root, Color.WHITE);
			
			stage.setTitle("Station A");
			
			stage.setScene(scene1);
			stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}