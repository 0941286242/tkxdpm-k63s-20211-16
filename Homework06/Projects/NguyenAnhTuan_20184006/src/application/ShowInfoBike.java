package application;

import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;


public class ShowInfoBike implements Initializable {
	
	@FXML
    private TextField phoneText;

    @FXML
    private Label phoneLabel;

    @FXML
    private Label timeEndLabel;

    @FXML
    private TextField emailText;

    @FXML
    private Label timeStartLabel;

    @FXML
    private TextField nameText;

    @FXML
    private Label emailLabel;

    @FXML
    private TextField timeEndText;

    @FXML
    private Label nameLabel;

    @FXML
    private TextField timeStartText;
    
    @FXML
    private Label idLabel;

    @FXML
    private Label idText;
    
    public static Connection getMyConnection() throws SQLException,
    ClassNotFoundException {
		return MySQLConnection.getMySQLConnection();
   } 
    
    public void getDataInfoBike() throws SQLException,
    ClassNotFoundException {
    	Connection conn = ShowInfoBike.getMyConnection();
    	PreparedStatement stmt = conn.prepareStatement("select * from infobike where bikeid = ?");
    	stmt.setInt(1, Integer.parseInt(idText.getText()));
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()) {
	    	nameText.setText(rs.getString(2));
			phoneText.setText(rs.getString(3)); 
	    	emailText.setText(rs.getString(4));
	    	timeStartText.setText(rs.getDate(5).toString());
	    	timeEndText.setText(rs.getDate(6).toString());
    	}
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	try {
    		getDataInfoBike();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	
}
