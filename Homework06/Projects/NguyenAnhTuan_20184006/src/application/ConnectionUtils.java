package application;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class ConnectionUtils {

  public static Connection getMyConnection() throws SQLException,
          ClassNotFoundException {
      return MySQLConnection.getMySQLConnection();
  }


  public static void main(String[] args) throws SQLException,
          ClassNotFoundException {

      System.out.println("Get connection ... ");

      Connection conn = ConnectionUtils.getMyConnection();

      System.out.println("Get connection " + conn);
      
      Statement stmt = conn.createStatement();
      
      ResultSet rs = stmt.executeQuery("select * from infobike");
      
      while (rs.next()) {
          System.out.println(rs.getInt(1) + "  " + rs.getString(2) 
                  + "  " + rs.getString(3) + "  " + rs.getString(4) + "  " + rs.getDate(5) + "  " + rs.getDate(6) );
      }
  }

}