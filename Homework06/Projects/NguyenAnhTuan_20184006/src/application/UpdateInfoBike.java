package application;

import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;


public class UpdateInfoBike implements Initializable {
	
	@FXML
    private TextField phoneText;

    @FXML
    private Label phoneLabel;

    @FXML
    private Label timeEndLabel;

    @FXML
    private TextField emailText;

    @FXML
    private Label timeStartLabel;

    @FXML
    private TextField nameText;

    @FXML
    private Button confirmButton;

    @FXML
    private Label emailLabel;

    @FXML
    private TextField timeEndText;

    @FXML
    private Label nameLabel;

    @FXML
    private TextField timeStartText;
    
    @FXML
    private Label idLabel;

    @FXML
    private Label idText;
    
    public static Connection getMyConnection() throws SQLException,
    ClassNotFoundException {
		return MySQLConnection.getMySQLConnection();
    } 
    
    public void updateDataInfoBike() throws SQLException,
    ClassNotFoundException {
    	Connection conn = ShowInfoBike.getMyConnection();
    	PreparedStatement st = conn.prepareStatement("UPDATE infobike SET name = ?, phone = ?, email = ?, timestart = ?, timeend = ? WHERE bikeid = ?");
    	st.setString(1, nameText.getText());
    	st.setString(2, phoneText.getText());
    	st.setString(3, emailText.getText());
    	st.setString(4, timeStartText.getText());
    	st.setString(5, timeEndText.getText());
    	st.setInt(6, Integer.parseInt(idText.getText()));
    	st.executeUpdate();
    	PreparedStatement st2 = conn.prepareStatement("UPDATE statusbike SET status = ? WHERE bikeid = ?");
    	st2.setString(1, "true");
    	st2.setInt(2, Integer.parseInt(idText.getText()));
    	st2.executeUpdate();
    }
    
    public void changeStatus() throws SQLException,
    ClassNotFoundException {
    	Connection conn = ShowInfoBike.getMyConnection();
    	PreparedStatement st = conn.prepareStatement("UPDATE statusbike SET status = ? WHERE bikeid = ?");
    	st.setString(1, "true");
    	st.setInt(2, Integer.parseInt(idText.getText()));
    	st.executeUpdate();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	phoneText.setText(null); 
    	nameText.setText(null);
    	emailText.setText(null);
    	timeStartText.setText(null);
    	timeEndText.setText(null);
    }
    
    
    public void confirm(ActionEvent e) {
    	if(phoneText.getText() != null && nameText.getText() != null && emailText.getText() != null && timeStartText.getText() != null && timeEndText.getText() != null) {
    		try {
    			updateDataInfoBike();
    			changeStatus();
    		} catch (ClassNotFoundException | SQLException error) {
    			// TODO Auto-generated catch block
    			error.printStackTrace();
    		}
    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setHeaderText("Thêm thành công!");
			alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea("Error")));
			alert.showAndWait();
    	} else {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setHeaderText("Nhập đủ các trường yêu cầu!");
			alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea("Error")));
			alert.showAndWait();
    	}
    }
	
}
