package application.entity.bike;

import application.database.ConnectDatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

public class Bike {
    protected int barcode;
    protected int station;
    protected String type;
    protected float weight;
    protected String license;
    protected LocalDate dateOfManufacture;
    protected String producer;
    protected float depositValue;
    protected float cost;
    protected boolean available;
    
    public Bike(int barcode, String type, float depositValue) {
        this.barcode = barcode;
        this.type = type;
        this.depositValue = depositValue;
    }

    public boolean checkBikeAvailable(){
        try {
            Statement statement = ConnectDatabase.getConnection().createStatement();
            String query = "select avalable from Bike where barcode = " + barcode;
            ResultSet rs = statement.executeQuery(query);
            int res = 0;
            while (rs.next()) {
                res = rs.getInt(1);
            }
            if (res == 1)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	public String getType() {
		return license;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public LocalDate getDateOfManufacture() {
		return dateOfManufacture;
	}

	public void setDateOfManufacture(LocalDate dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	public void setBarcode(int barcode) {
		this.barcode = barcode;
	}

	public int getBarcode() {
		return this.barcode;
	}

	public void setStation(int station) {
		this.station = station;
	}

	public float getDepositValue() {
		return this.depositValue;
	}
	
	public void setDepositValue(float depositValue) {
		this.depositValue = depositValue;
	}
}