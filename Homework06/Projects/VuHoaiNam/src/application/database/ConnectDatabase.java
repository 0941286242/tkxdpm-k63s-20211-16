package application.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDatabase {
    private static String DB_URL = "jdbc:mysql://localhost/bike_rent_eco";
    private static String USER_NAME = "root";
    private static String PASSWORD = "123456";

    private static Connection connection;

    public static Connection getConnection() {
        if (connection == null) {
            try {
            	Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
                System.out.println("Connected");
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return connection;
    }

    public static void close() throws SQLException {
        if (!connection.isClosed())
            connection.close();
    }
}
