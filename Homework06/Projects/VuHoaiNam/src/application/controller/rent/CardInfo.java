package application.controller.rent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class CardInfo {
	@FXML
	TextField name;
    @FXML
    TextField cardId;
    @FXML
    TextField cmnd;
    @FXML
    PasswordField password;
    
    public void onPayment(ActionEvent event) {
    	System.out.println("1".equals(new String(cardId.getText())));
    	String cardRight = "1";
    	String cardNotEnoughBalance = "2";
    	String passwordRight = "1";
    	if (cardNotEnoughBalance.equals(new String(cardId.getText()))) {
    		Alert alert = new Alert(AlertType.ERROR);

    		alert.setTitle("Error alert");
    		alert.setHeaderText("Not enough balance");

    		alert.showAndWait();
    	} else if (cardRight.equals(new String(cardId.getText())) && passwordRight.equals(new String(password.getText()))) {
    		Alert alert = new Alert(AlertType.INFORMATION);
    		alert.setTitle("Success alert");
    		alert.setHeaderText("Payment successfully");

    		alert.showAndWait();
    	} else {
    		Alert alert = new Alert(AlertType.ERROR);

    		alert.setTitle("Error alert");
    		alert.setHeaderText("Can not payment");
    		alert.setContentText("The card information wrong!");

    		alert.showAndWait();
    	}
    }
    
    public void onCancel() {
    	
    }
}
