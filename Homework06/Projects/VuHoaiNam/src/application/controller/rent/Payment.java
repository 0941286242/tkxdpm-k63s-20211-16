package application.controller.rent;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class Payment {
	@FXML
	Label depositValue;
    @FXML
    Label barcode;
    
	public void setInfoBike(String barcodeBike, String depositValueBike) {
		barcode.setText(String.valueOf(barcodeBike));
		depositValue.setText(String.valueOf(depositValueBike));
	}
	
	public void paymentWithBidv(ActionEvent event) throws IOException {
		Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/application/views/fxml/rent/cardInfo.fxml"));
        Parent studentViewParent = loader.load();
        Scene scene = new Scene(studentViewParent);
        stage.setScene(scene);
	}
	
	public void paymentWithVcb() {
		
	}
}
