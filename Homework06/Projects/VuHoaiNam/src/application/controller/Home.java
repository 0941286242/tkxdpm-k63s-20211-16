package application.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Home implements Initializable {
   @FXML
   private Button bike_management;
   
   @Override
   public void initialize(URL location, ResourceBundle resources) {

       // TODO (don't really need to do anything here).
       // TODO (Thực sự cũng không cần thiết viết gì ở đây).
     
   }
   
   public void redirectBikeManagement(ActionEvent event) throws IOException {
	   System.out.println("Button Clicked!");
	  
       Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
       FXMLLoader loader = new FXMLLoader();
       loader.setLocation(getClass().getResource("/application/views/fxml/bike_management/bikeManagement.fxml"));
       Parent studentViewParent = loader.load();
       Scene scene = new Scene(studentViewParent);
       stage.setScene(scene);
   }
}
