package application.controller.bike_management;

import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.database.ConnectDatabase;
import application.entity.bike.Bike;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class BikeManagement implements Initializable {

   @FXML
   private Button myButton;
 
   @FXML
   private TableView bikeManagementTable;
   @FXML
   private TableColumn stt;
   @FXML
   private TableColumn barcode;
   @FXML
   private TableColumn type;
   @FXML
   private TableColumn depositValue;
   @FXML
   private TableColumn action;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
	   System.out.println("Bike Management");
//	   stt.setCellValueFactory(new PropertyValueFactory<>("stt"));
	   barcode.setCellValueFactory(new PropertyValueFactory<Bike, Integer>("barcode"));
	   type.setCellValueFactory(new PropertyValueFactory<Bike, String>("type"));
	   depositValue.setCellValueFactory(new PropertyValueFactory<Bike, Float>("depositValue"));
	   action.setCellValueFactory(new PropertyValueFactory<>(""));

       Callback<TableColumn<Bike, String>, TableCell<Bike, String>> cellFactory
               = //
               new Callback<TableColumn<Bike, String>, TableCell<Bike, String>>() {
           @Override
           public TableCell call(final TableColumn<Bike, String> param) {
               final TableCell<Bike, String> cell = new TableCell<Bike, String>() {

                   final Button btn = new Button("Chi tiet");

                   @Override
                   public void updateItem(String item, boolean empty) {
                       super.updateItem(item, empty);
                       if (empty) {
                           setGraphic(null);
                           setText(null);
                       } else {
                           btn.setOnAction(event -> {
                        	   Bike bike = getTableView().getItems().get(getIndex());
                        	   FXMLLoader fxmlLoader = new 
                                       FXMLLoader(getClass().getResource("/application/views/fxml/bike_management/bikeDetail.fxml"));
                               Parent root1 = null;
								try {
									root1 = (Parent) fxmlLoader.load();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                               Stage stage = new Stage();
                               //set what you want on your stage
                               stage.initModality(Modality.APPLICATION_MODAL);
                               stage.setTitle("Bike Detail");
                               BikeDetail controller = fxmlLoader.getController();
                               System.out.println("Button Clicked!"+ bike.getBarcode());
                               try {
									controller.setBike(bike.getBarcode());
                               } catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
                               }
                               stage.setScene(new Scene(root1));
                               stage.setResizable(false);
                               stage.show();
                           });
                           setGraphic(btn);
                           setText(null);
                       }
                   }
               };
               return cell;
           }
       };

       action.setCellFactory(cellFactory);
       
	   bikeManagementTable.getColumns().addAll(barcode, type, depositValue, action);
	   
	   
	   try {
           Statement statement = ConnectDatabase.getConnection().createStatement();
           String query = "SELECT barcode, type, depositValue FROM `bike`";
           ResultSet rs = statement.executeQuery(query);
           int res = 0;
           while (rs.next()) {
               res = rs.getRow();
               Bike bike = new Bike(rs.getInt(1), rs.getString(2), rs.getFloat(3));

               System.out.println("res" + rs.getInt(1) +rs.getString(2)+ rs.getFloat(3));
               bikeManagementTable.getItems().add(bike);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
	   
   }
}
