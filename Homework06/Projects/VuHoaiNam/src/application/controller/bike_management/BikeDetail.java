package application.controller.bike_management;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import application.controller.rent.Payment;
import application.database.ConnectDatabase;
import application.entity.bike.Bike;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class BikeDetail {
	
	@FXML
	Button rentBike;
    @FXML
    Label barcode;
    @FXML
    Label type;
    @FXML
    Label weight;
    @FXML
    Label license;
    @FXML
    Label dateOfManufacture;
    @FXML
    Label producer;
    @FXML
    Label depositValue;

    public void setBike(int barcodeBike) throws SQLException{
    	System.out.println("Barcode!" + barcodeBike);
    	Statement statement = ConnectDatabase.getConnection().createStatement();
    	String query = "SELECT * FROM `bike` WHERE barcode = " + barcodeBike;
    	ResultSet rs = statement.executeQuery(query);
    	while (rs.next()) {
    		System.out.println("bike!" + String.valueOf(rs.getInt(1))+ rs.getString(3)+ String.valueOf(rs.getInt(4)));
           	barcode.setText(String.valueOf(rs.getInt(1)));
   	        type.setText(rs.getString(3));
   	        weight.setText(String.valueOf(rs.getInt(4)));
   	        license.setText(rs.getString(5));
   	        dateOfManufacture.setText(rs.getString(6));
   	        producer.setText(rs.getString(7));
   	        depositValue.setText(String.valueOf(rs.getInt(8)));
    	}
	}
    
    public void onRentBike(ActionEvent event) throws IOException {
    	 Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
         FXMLLoader loader = new FXMLLoader();
         loader.setLocation(getClass().getResource("/application/views/fxml/rent/paymentView.fxml"));
         Parent studentViewParent = loader.load();
         Payment controller = loader.getController();
         controller.setInfoBike(barcode.getText(), depositValue.getText());
         Scene scene = new Scene(studentViewParent);
         stage.setScene(scene);
    }
}
