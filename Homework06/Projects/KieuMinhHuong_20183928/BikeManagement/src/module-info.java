module BikeManagement {
	requires javafx.controls;
	requires javafx.graphics;
	requires java.sql;
	requires javafx.fxml;
	requires java.desktop;
	
	opens controllers to javafx.graphics, javafx.fxml;
	opens models to javafx.base;
}
