package controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.ResourceBundle;

import helpers.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import models.Bike;
import models.Station;

public class AddBikeController implements Initializable{

	@FXML
	private ComboBox<String> bikeType;
	@FXML
	private TextArea bikeDescription;
	
	public void initialize(URL location, ResourceBundle resourses) {
		ObservableList<String> bikeTypeList = FXCollections.observableArrayList("Xe đạp đơn thường", "Xe đạp đơn điện", "Xe đạp đôi thường");
		bikeType.setItems(bikeTypeList);
		
	}
	
	public void returnToBikeView(ActionEvent event) throws Exception{
		
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/BikeView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	public void addBike(ActionEvent event) throws Exception {
		DatabaseConnection connection = new DatabaseConnection();
		Connection connectDB = connection.getConnection();
		String choosedStationName = StationController.choosedStationName;
		String query = "SELECT id FROM station WHERE name = '" + choosedStationName +"'";
		int stationId = 0;
		try {
			Statement statement = connectDB.createStatement();
			ResultSet stationOutput = statement.executeQuery(query);
			while(stationOutput.next()) {
				stationId = stationOutput.getInt(1);
			}
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Random generator = new Random();
		int id = generator.nextInt(9999);
		int type = bikeType.getSelectionModel().getSelectedIndex();
		if(type == -1) type = 0;
		type += 1;
		if (bikeDescription.getText().length() >= 50) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("LỖI SỬA MÔ TẢ");
			alert.setContentText("Mô tả không được vượt quá 50 ký tự.");
			alert.showAndWait();
			return;
		}

		query = "INSERT INTO bike (id, type, status, station_id, description) VALUES (" + id + "," + type + ",0," + stationId + ",'" + bikeDescription.getText() + "')";
		String query2 = "SELECT total_bike, available_bike FROM station WHERE id = " + stationId;
		try {
			Statement statement = connectDB.createStatement();
			statement.executeUpdate(query);
			ResultSet stationOutput = statement.executeQuery(query2);
			int totalBike = 0, availableBike = 0;
			while(stationOutput.next()) {
				totalBike = stationOutput.getInt(1);
				availableBike = stationOutput.getInt(2);
			}
			totalBike++;
			availableBike++;
			query2 = "UPDATE station SET total_bike = " + totalBike + ", available_bike = " + availableBike + " WHERE id = " + stationId;
			statement.executeUpdate(query2);
			returnToBikeView(event);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	public void cancel(ActionEvent event) throws Exception {
		returnToBikeView(event);
	}

}
