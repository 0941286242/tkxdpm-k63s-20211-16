package controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import org.w3c.dom.NameList;

import helpers.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import models.Station;

public class StationController implements Initializable{
	
	@FXML
	private TableView<Station> stationTable;
	@FXML
	private TableColumn<Station, String> nameColumn;
	@FXML
	private TableColumn<Station, String> addressColumn;
	@FXML
	private TableColumn<Station, Integer> totalBikeColumn;
	@FXML
	private TableColumn<Station, Integer> availableBikeColumn;
	@FXML
	public ObservableList<Station> stationList = FXCollections.observableArrayList();
	
	public static String choosedStationName;

	public static String getChoosedStationName() {
		return choosedStationName;
	}

	public void setChoosedStationName(String choosedStationName) {
		this.choosedStationName = choosedStationName;
	}

	public void initialize(URL location, ResourceBundle resourses) {
		DatabaseConnection connection = new DatabaseConnection();
		Connection connectDB = connection.getConnection();
		
		String query = "SELECT * FROM station";
		
		try {
			Statement statement = connectDB.createStatement();
			ResultSet stationOutput = statement.executeQuery(query);
			
			while(stationOutput.next()) {
				Station temp = new Station(stationOutput.getString(2), stationOutput.getString(3), stationOutput.getInt(4), stationOutput.getInt(5));
				stationList.add(temp);	
			}
			
			nameColumn.setCellValueFactory(new PropertyValueFactory<Station, String>("name"));
			addressColumn.setCellValueFactory(new PropertyValueFactory<Station, String>("address"));
			totalBikeColumn.setCellValueFactory(new PropertyValueFactory<Station, Integer>("totalBike"));
			availableBikeColumn.setCellValueFactory(new PropertyValueFactory<Station, Integer>("availableBike"));
			
			stationTable.setItems(stationList);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void chooseStation(ActionEvent event) throws Exception{
		Station choosed = stationTable.getSelectionModel().getSelectedItem();
		
		setChoosedStationName(choosed.getName());
		
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/BikeView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
}
