package controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import helpers.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import models.Bike;

public class BikeController implements Initializable{
	
	@FXML
	private TableView<Bike> bikeTable;
	@FXML
	private TableColumn<Bike, Integer> idColumn;
	@FXML
	private TableColumn<Bike, String> typeColumn;
	@FXML
	private TableColumn<Bike, String> statusColumn;
	@FXML
	private TableColumn<Bike, String> descriptionColumn;
	@FXML
	public ObservableList<Bike> BikeList = FXCollections.observableArrayList();
	@FXML
	private Button choosedBike;
	@FXML
	private Label title;
	
	public static int choosedBikeId;
	
	public void initialize(URL location, ResourceBundle resourses) {
		DatabaseConnection connection = new DatabaseConnection();
		Connection connectDB = connection.getConnection();
		String choosedStationName = StationController.choosedStationName;
		title.setText(choosedStationName);
		String query = "SELECT bike.id, bike.type, bike.status, bike.description FROM bike INNER JOIN station ON bike.station_id = station.id WHERE station.name = '" + choosedStationName + "'";
		
		try {
			Statement statement = connectDB.createStatement();
			ResultSet BikeOutput = statement.executeQuery(query);
			
			while(BikeOutput.next()) {
				String bikeType;
				String bikeStatus;
				if(BikeOutput.getInt(2) == 1) {
					bikeType = "Xe đạp đơn thường";
				} else if (BikeOutput.getInt(2) == 2) {
					bikeType = "Xe đạp đơn điện";
				} else {
					bikeType = "Xe đạp đôi thường";
				}
				
				if(BikeOutput.getInt(3) == 0) {
					bikeStatus = "Xe chưa được thuê";
				} else {
					bikeStatus = "Xe đã được thuê";
				}
				Bike temp = new Bike(BikeOutput.getInt(1), bikeType, bikeStatus, BikeOutput.getString(4));
				BikeList.add(temp);	
			}
			
			idColumn.setCellValueFactory(new PropertyValueFactory<Bike, Integer>("id"));
			typeColumn.setCellValueFactory(new PropertyValueFactory<Bike, String>("bikeType"));
			statusColumn.setCellValueFactory(new PropertyValueFactory<Bike, String>("bikeStatus"));
			descriptionColumn.setCellValueFactory(new PropertyValueFactory<Bike, String>("description"));
			
			bikeTable.setItems(BikeList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void returnStation(ActionEvent event) throws Exception {
		
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/StationView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void addBike(ActionEvent event) throws Exception {
		
		try {
			
			Parent root = FXMLLoader.load(getClass().getResource("/views/AddBikeView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void fixBike(ActionEvent event) throws Exception {
		Bike choosedBike = bikeTable.getSelectionModel().getSelectedItem();
		choosedBikeId = choosedBike.getId();
		try {
			
			Parent root = FXMLLoader.load(getClass().getResource("/views/FixBikeView.fxml"));
			Stage newStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			Scene newScene = new Scene(root);
			newStage.setScene(newScene);
			newStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void deleteBike() {
		Bike choosedBike = bikeTable.getSelectionModel().getSelectedItem();
		DatabaseConnection connection = new DatabaseConnection();
		Connection connectDB = connection.getConnection();
		String query = "DELETE FROM bike where id =" + choosedBike.getId();
		String query2 = "SELECT total_bike, available_bike FROM station WHERE name = '" + StationController.choosedStationName + "'";
		try {
			Statement statement = connectDB.createStatement();
			int BikeOutput = statement.executeUpdate(query);
			ResultSet stationOutput = statement.executeQuery(query2);
			int totalBike = 0, availableBike = 0;
			while(stationOutput.next()) {
				totalBike = stationOutput.getInt(1);
				availableBike = stationOutput.getInt(2);
			}
			totalBike--;
			availableBike--;
			query2 = "UPDATE station SET total_bike = " + totalBike + ", available_bike = " + availableBike + " WHERE name = '" + StationController.choosedStationName + "'";
			statement.executeUpdate(query2);
			BikeList.remove(choosedBike);

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}


}
